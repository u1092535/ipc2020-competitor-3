Bootstrap: docker
From: ubuntu:20.04

%files
	./planner /planner	

%setup
     ## The "%setup"-part of this script is called to bootstrap an empty
     ## container. It copies the source files from the branch of your
     ## repository where this file is located into the container to the
     ## directory "/planner". Do not change this part unless you know
     ## what you are doing and you are certain that you have to do so.

    #REPO_ROOT=`dirname $SINGULARITY_BUILDDEF`
    #cp -r $REPO_ROOT/ $SINGULARITY_ROOTFS/planner

%post

    ## The "%post"-part of this script is called after the container has
    ## been created with the "%setup"-part above and runs "inside the
    ## container". Most importantly, it is used to install dependencies
    ## and build the planner. Add all commands that have to be executed
    ## once before the planner runs in this part of the script.

    ## Install all necessary dependencies.
    apt-get update
    apt-get install --no-install-recommends -y \
	    bison \
	    build-essential \
	    curl \
	    ca-certificates \
	    flex \
	    g++-multilib \
	    gcc-multilib \
      git \
      haskell-platform \
      haskell-stack \
	    libboost-dev \
	    libboost-program-options-dev \
	    libjudy-dev \
      netbase \
	    python3 \
	    scons \
	    xutils-dev \
      && rm -rf /var/lib/apt/lists/*

    stack upgrade
    # Should set the commit hash at some point
    git clone -b ipc2020-devel https://github.com/ronwalf/HTN-Translation.git
    cd HTN-Translation
    stack install


    ## go to directory and make the planner
    cd /planner
    # Should set the commit hash at some point
    git clone https://github.com/LAPKT-dev/LAPKT-public.git lapkt
    # Stolen from lapkt's Dockerfile
    cd lapkt \
	    && cd external/libff \
	    && make clean && make depend && make \
	    && cd ../../ && mkdir compiled_planners && cd planners \
	    && cd siw_plus-ffparser && scons && cp siw_plus ../../compiled_planners/. && cd .. \
	    && cd siw-ffparser && scons && cp siw ../../compiled_planners/. && cd ..\
	    && cd bfs_f-ffparser && scons && cp bfs_f ../../compiled_planners/. && cd ..\
	    && cd dfs_plus-ffparser && scons && cp dfs_plus ../../compiled_planners/. && cd ..\
	    && cd ff-ffparser && scons && cp ff ../../compiled_planners/. && cd ..\
	    && cd siw_plus-then-bfs_f-ffparser && scons && cp siw-then-bfsf ../../compiled_planners/.

    cd /planner
    #make


%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    DOMAINFILE=$1
    PROBLEMFILE=$2
    PLANFILE=$3

    stdbuf -o0 -e0 /planner/plan $DOMAINFILE $PROBLEMFILE 2>&1 | tee $PLANFILE


## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name        HTN-Translation
Description 
Authors     Ron Alford <ronwalf@volus.net>
SupportsRecursion yes
SupportsPartialOrder yes
